<?php

/**
 * Plugin Name: Wen Ratings
 */
require 'wen_super_plugin.php';

define('TABLE', 'wen_reviews');

class wen_ratings
{

    public $params = array('post_id' => '', 'rating' => '', 'review' => '');

    public $htmlAllowed = array('p' => array(), 'a' => array(), 'i' => array(), 'h3' => array());

    public $errorMessage = array('message' => 'You have already voted in this post');

    public $successMessage = array('message' => 'You have successfully rated in this post');

    public $loginErrorMessage = array('message' => 'Please login to rate this post');

    public $notAllowed = array('message' => 'Not Allowed to rate this post');

    public $max = 5;

    public $min = 1;

    public $options;

    public $attributes = array(
        'non_star' => "fa fa-star-o",
        'star' => "fa fa-star",
        'before' => "<div>",
        'after' => "</div>"
    );

    public function activation()
    {
        global $wpdb;
        $charset = $wpdb->get_charset_collate();
        $table_name = $wpdb->prefix . TABLE;
        $sql = "CREATE TABLE $table_name (
        id mediumint(9) NOT NULL AUTO_INCREMENT,
        email TEXT NOT NULL,
        post_id INT NOT NULL,
        rating INT NOT NULL,
        review text,
        PRIMARY KEY  (id)
        ) $charset;";
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
    }

    public function deactivation()
    {

    }

    public function listen()
    {
//        add_action('init', array($this, 'rate'));
    }


    public function canRate()
    {

        return array_key_exists('rating', $_REQUEST) &&
            $_REQUEST['rating'] >= $this->min &&
            $_REQUEST['rating'] <= $this->max &&
            !is_admin() &&
            $this->checkLogin() &&
            $this->checkAbility();
    }

    public function checkLogin()
    {
        if (!is_user_logged_in()) {
            wp_send_json_error($this->loginErrorMessage, 401);
        }
        return true;
    }

    public function checkAbility()
    {
        if ($this->options[get_post_type($_REQUEST['post_id'])] != 1) {
            wp_send_json_error($this->notAllowed, 403);
        }
        return true;
    }

    public function boot()
    {
        $this->options = get_option('wen_settings');
        $this->options['before'] ? $this->attributes['before'] = $this->options['before'] : '';
        $this->options['after'] ? $this->attributes['after'] = $this->options['after'] : '';
        $this->options['star'] ? $this->attributes['star'] = $this->options['star'] : '';
        $this->options['non_star'] ? $this->attributes['non_star'] = $this->options['non_star'] : '';
        add_action('admin_menu', array($this, 'wen_add_admin_menu'));
        add_action('admin_init', array($this, 'wen_settings_init'));
        add_action('comment_post', array($this,'save_comment_meta_data'));
        add_action( 'comment_form_logged_in_after', array($this, 'custom_fields') );
        add_action( 'comment_form_after_fields',array($this, 'custom_fields') );
        add_shortcode('wr', array($this, 'shortcode'));
    }

    public function shortcode($atts)
    {
        $atts = shortcode_atts(array(
            'post_id' => is_array($atts) && array_key_exists('post_id', $atts) ? $atts['post_id'] : ''
        ), $atts, 'bartag');
        $id = get_the_ID();
        if ($atts['post_id']) {
            $id = $atts['post_id'];
        }
        global $wpdb;
        $table = $this->getTable($wpdb);
        $rating = ceil($wpdb->get_var("SELECT AVG(rating) from $table where post_id = $id"));
        $pad = $this->max - $rating;
        $html = "<div class='wen_rating'>";
        $html .= $this->attributes['before'];
        $range = range($this->min, $rating);
        $count = 0;
        array_walk($range, function ($n) use (&$html, &$count) {
            $html .= '<i data-id="' . ++$count . '" class="' . $this->attributes['star'] . '" > </i>';
        });
        if (count($range) < 5) {
            $range1 = range(count($range) + 1, $this->max);
            array_walk($range1, function ($n) use (&$html, &$count) {
                $html .= '<i data-id="' . ++$count . '" class="' . $this->attributes['non_star'] . '" > </i>';

            });
        }
        $html .= $this->attributes['after'];
        $html .= "</div>";
        return balanceTags($html, true);;
    }

    public function options($term)
    {
        return $this->option[$term];
    }

    public function getTable($db)
    {
        return $db->prefix . TABLE;
    }

    public function getAttribute($term)
    {
        return $this->attributes[$term];
    }

    public function wen_add_admin_menu()
    {

        add_options_page('Ratings By WenSolutions', 'Ratings By WenSolutions', 'manage_options', 'wen_ratings', array($this, 'wen_options_page'));

    }

    public function wen_settings_init()
    {

        register_setting('rating', 'wen_settings');

        add_settings_section(
            'wen_rating_section',
            __('Use [wr] as shortcode.', 'wen'),
            array($this, 'wen_settings_section_callback'),
            'rating'
        );

        add_settings_field(
            'wen_checkbox_field_0',
            __('Post Types', 'wen'),
            array($this, 'wen_checkbox_field_0_render'),
            'rating',
            'wen_rating_section'
        );
        add_settings_section(
            'wen_rating_html_section',
            __('Set html contents', 'wen'),
            array($this, 'wen_html_callback'),
            'rating'
        );

        add_settings_field(
            'wen_html_before_render',
            __('Before', 'wen'),
            array($this, 'wen_html_before_render'),
            'rating',
            'wen_rating_html_section'
        );
        add_settings_field(
            'wen_html_after_render',
            __('After', 'wen'),
            array($this, 'wen_html_after_render'),
            'rating',
            'wen_rating_html_section'
        );
        add_settings_field(
            'wen_html_star_render',
            __('Html for star', 'wen'),
            array($this, 'wen_html_star_render'),
            'rating',
            'wen_rating_html_section'
        );
        add_settings_field(
            'wen_html_non_star_render',
            __('Html for non star', 'wen'),
            array($this, 'wen_html_non_star_render'),
            'rating',
            'wen_rating_html_section'
        );


    }

    public function wen_html_before_render()
    {
        echo "<input type='text' name='wen_settings[before]' value='" . esc_html($this->options['before']) . "'/>";
    }

    public function wen_html_after_render()
    {
        echo "<input type='text' name='wen_settings[after]' value='" . esc_html($this->options['after']) . "'/>";
    }

    public function wen_html_star_render()
    {
        echo "<input type='text' name='wen_settings[star]' value='" . esc_html($this->options['star']) . "'/>";
    }

    public function wen_html_non_star_render()
    {
        echo "<input type='text' name='wen_settings[non_star]' value='" . esc_html($this->options['non_star']) . "'/>";
    }

    public function wen_checkbox_field_0_render()
    {

        $terms = get_post_types(array('public' => true));
        foreach ($terms as $term) {
            ?>
            <input type='checkbox' name='wen_settings[<?php echo $term; ?>]' <?php checked($this->options[$term], 1); ?>
                   value='1'> <?php echo ucfirst(str_replace('_', ' ', $term)); ?> <br>
            <?php
        }


    }

    public function wen_settings_section_callback()
    {

        // echo __( 'Shortcode is [wr]', 'wen' );

    }

    public function wen_options_page()
    {

        ?>
        <form action='options.php' method='post'>

            <h2>Wen Ratings Settings </h2>

            <?php
            settings_fields('rating');
            do_settings_sections('rating');
            submit_button();
            ?>

        </form>
        <?php

    }

    public function save_comment_meta_data($comment_id)
    {
        $requests = array_intersect_key($_REQUEST, $this->params);
        $email = $_REQUEST['email'];
        if(is_user_logged_in()){
            $email = get_userdata( get_current_user_id() )->email;
        }
        foreach ($requests as $key => $request) {
            $requests[$key] = wp_kses(sanitize_text_field($request), $this->htmlAllowed);
        }
        global $wpdb;
        $post_id = $_REQUEST['comment_post_ID'];
        $table = $wpdb->prefix . TABLE;
        if ($wpdb->get_var("SELECT count(*) from $table where email = '$email' and post_id = $post_id;") > 0) {
            $wpdb->update($table, array('rating' => $_REQUEST['rating']), array('post_id' => $post_id, 'email' => $email));
        } else {
            $wpdb->insert($table, ['email'=>$email,'post_id'=>$post_id,'rating'=> $_REQUEST['rating']]);
        }
    }

    public function custom_fields($fields)
    {

        $rating = '<div class="reviewer">';

        foreach (range($this->min, $this->max) as $range) {
            $rating .= '<i data-id="' . $range . '" class="' . $this->attributes['non_star'] . '" > </i>';
        }
        $rating .= "</div>";

        $rating = '
        <div class="reviewer">
        <input type="hidden" name="rating" value="0">
        Rating
        ' . $rating . '
        </div><br>
        ';
        echo $rating;
    }
}

$plugin = new wen_ratings();
$plugin->boot();
$plugin->listen();

register_activation_hook(__FILE__, array('wen_ratings', 'activation'));

